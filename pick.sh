#!/bin/bash

set -e
source "build/envsetup.sh";
source "vendor/aosp/build/envsetup.sh";

# frameworks/av
changes=(
351261 # SessionConfigurationUtils: Allow privileged camera apps to create raw streams for raw capable cameras
)
repopick -g https://review.lineageos.org  -P frameworks/av ${changes[@]}&

# frameworks/base
changes=(
351262 # Camera: Expose aux camera if packagename is null
)
repopick -g https://review.lineageos.org  -P frameworks/base ${changes[@]}&

# frameworks/opt/telephony
changes=(
349338 # Disable proguard for CellularNetworkServiceProvider
349339 # Add provision to override CellularNetworkService
349340 # Make a few  members of DSM overridable and accessible
349341 # Reset data activity after traffic status poll stops
349342 # Start using inject framework support
349343 # Skip sending duplicate requests
349344 # Enable vendor Telephony plugin: MSIM Changes
)
repopick -g https://review.lineageos.org  -P frameworks/opt/telephony ${changes[@]}&

# hardware/interfaces
changes=(
349303 # frameworks: Update HIDL overrideFormat from HAL
)
repopick -g https://review.lineageos.org  -P hardware/interfaces ${changes[@]}